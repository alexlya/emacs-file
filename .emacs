(tool-bar-mode 0)			; disable tool bar
(menu-bar-mode 0)			; disable menu bar
(scroll-bar-mode 0)			; disable scroll bar
(setq backup-inhibited t)		; disable backup
(setq auto-save-default nil)		; disable auto save
(setq inhibit-splash-screen t)		; disable splash screen
(setq initial-scratch-message "")	; remove message from scratch
(add-hook 'org-mode-hook 'turn-on-auto-fill) ; automatically wrap paragraph

;; this function pastes current date 
(defun date (arg)
   (interactive "P")
   (insert (if arg
               (format-time-string "%d.%m.%Y")
             (format-time-string "%Y-%m-%d"))))

;; this function pastes current time
(defun time (arg)
   (interactive "P")
   (insert (if arg
               (format-time-string "%d.%m.%Y")
             (format-time-string "%T"))))

;; this function pastes current datetime 
(defun datetime (arg)
   (interactive "P")
   (insert (if arg
               (format-time-string "%d.%m.%Y")
             (format-time-string "%Y-%m-%d %T"))))

;; Default folder.
(setq default-directory "/path_to_your_folder/" )

;; Turn on autofill.
(add-hook 'text-mode-hook 'turn-on-auto-fill)
